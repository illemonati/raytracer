
use crate::sphere::Sphere;
use glam::Vec3;

pub fn nearest_intersected_sphere<'a>(spheres: &'a [Sphere], ray_origin: &'a Vec3, ray_direction: &'a Vec3) -> (Option<&'a Sphere>, f32) {
    let mut nearest_sphere = None;
    let mut min_distance = f32::INFINITY;

    for sphere in spheres {
        let distance = sphere.first_intersection(ray_origin, ray_direction);
        if distance > 0.0 && distance < min_distance {
            min_distance = distance;
            nearest_sphere = Some(sphere);
        }
    }

    (nearest_sphere, min_distance)
}

pub fn reflected(vector: &Vec3, axis: &Vec3) -> Vec3 {
    *vector - 2.0 * vector.dot(*axis) * *axis
}