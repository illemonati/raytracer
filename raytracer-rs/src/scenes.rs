use crate::sphere::Sphere;
use glam::Vec3;
use rand::Rng;
use crate::light::Light;


pub fn many_spheres_two_lights_scene() ->  (Vec<Sphere>, Vec<Light>) {
    let (spheres, mut lights) = many_spheres_scene();

     lights.push(Light::new(
            Vec3::new(-2.0, 5.0, 3.0),
            Vec3::new(0.5, 0.5, 0.5),
            Vec3::new(0.5, 0.5, 0.5),
            Vec3::new(0.5, 0.5, 0.5),
        )
    );

    (spheres, lights)
}


pub fn many_spheres_scene() -> (Vec<Sphere>, Vec<Light>) {
    let mut rng = rand::thread_rng();
    let mut spheres: Vec<Sphere> = vec![
         Sphere::new(
            9000.0-0.7, 
            Vec3::new(0.0, -9000.0, 0.0),
            Vec3::new(0.1, 0.1, 0.1),
            Vec3::new(0.6, 0.6, 0.6),
            Vec3::new(1.0, 1.0, 1.0),
            100.0,
            0.5
        )
    ];

    let lights: Vec<Light> = vec![
         Light::new(
            Vec3::new(5.0, 5.0, 5.0),
            Vec3::new(1.0, 1.0, 1.0),
            Vec3::new(1.0, 1.0, 1.0),
            Vec3::new(1.0, 1.0, 1.0),
        ),
    ];

    for row in 0..3 {
        let x = -0.8 + (row as f32 * (1.5--0.8)/3.0);
        for col in 0..3 {
            let y = -0.5 + (col as f32 * (1.0--0.5)/3.0);
            for file in 0..2 {
                let z = -0.5 + (file as f32 * (0.5--0.5)/2.0);

                spheres.push(Sphere::new(
                    0.2,
                    Vec3::new(x, y, z),
                    Vec3::new(rng.gen_range(0.0..=0.1), rng.gen_range(0.0..=0.1), rng.gen_range(0.0..=0.1)),
                    Vec3::new(rng.gen_range(0.0..=1.0), rng.gen_range(0.0..=1.0), rng.gen_range(0.0..=1.0)),
                    Vec3::new(1.0, 1.0, 1.0),
                    100.0,
                    0.5,
                ))
            }
        }
    }
    (spheres, lights)
} 


pub fn default_scene_two_lights() -> (Vec<Sphere>, Vec<Light>) {
    let (spheres, lights) = default_scene();
    let spheres = Vec::from(spheres);
    let mut lights = Vec::from(lights);

    lights.push(Light::new(
            Vec3::new(0.0, 5.0, 3.0),
            Vec3::new(0.5, 0.5, 0.5),
            Vec3::new(0.5, 0.5, 0.5),
            Vec3::new(0.5, 0.5, 0.5),
        )
    );

    (spheres, lights)
}

pub fn default_scene() -> ([Sphere; 4], [Light; 1]) {
    let spheres = [
        Sphere::new(
            0.7,
            Vec3::new(-0.2, 0.0, -1.0),
            Vec3::new(0.1, 0.0, 0.0),
            Vec3::new(0.7, 0.0, 0.0),
            Vec3::new(1.0, 1.0, 1.0),
            100.0,
            0.5
        ),
        Sphere::new(
            0.1,
            Vec3::new(0.1, -0.3, 0.0),
            Vec3::new(0.1, 0.0, 0.1),
            Vec3::new(0.7, 0.0, 0.7),
            Vec3::new(1.0, 1.0, 1.0),
            100.0,
            0.5
        ),
        Sphere::new(
            0.15,
            Vec3::new(-0.3, 0.0, 0.0),
            Vec3::new(0.0, 0.1, 0.0),
            Vec3::new(0.6, 0.6, 0.6),
            Vec3::new(1.0, 1.0, 1.0),
            100.0,
            0.5
        ),
        Sphere::new(
            9000.0-0.7, 
            Vec3::new(0.0, -9000.0, 0.0),
            Vec3::new(0.1, 0.1, 0.1),
            Vec3::new(0.6, 0.6, 0.6),
            Vec3::new(1.0, 1.0, 1.0),
            100.0,
            0.5
        )
    ];



    let lights: [Light; 1] = [
        Light::new(
            Vec3::new(5.0, 5.0, 5.0),
            Vec3::new(1.0, 1.0, 1.0),
            Vec3::new(1.0, 1.0, 1.0),
            Vec3::new(1.0, 1.0, 1.0),
        ),
    ];

    return (spheres, lights);
}