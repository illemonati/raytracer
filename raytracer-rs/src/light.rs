use glam::Vec3;



pub struct Light {
    pub center: Vec3,
    pub ambient: Vec3,
    pub diffuse: Vec3,
    pub specular: Vec3,
}


impl Light {
    pub fn new( center: Vec3, ambient: Vec3, diffuse: Vec3, specular: Vec3) -> Self {
        Self {
            center,
            ambient,
            diffuse,
            specular,
        }
    }
}