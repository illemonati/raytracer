mod sphere;
mod utils;
mod scenes;
mod light;

use glam::Vec3;
use image::{RgbImage, Rgb};
use utils::{nearest_intersected_sphere, reflected};


const SCREEN_WIDTH: u32 = 1800;
const SCREEN_HEIGHT: u32 = 1200;

const SCREEN_WIDTH_HEIGHT_RATIO: f32 = (SCREEN_WIDTH as f32) / (SCREEN_HEIGHT as f32);

const SCREEN_DEPTH: f32 = 0.0;

// 1 is no reflection
const MAX_REFLECTION_DEPTH: usize = 3;



fn main() {
    let camera_position: Vec3 = Vec3::new(0.0, 0.0, 1.0);

    let (screen_left, screen_top, screen_right, screen_bottom) = (
        -1.0f32,
        1.0/SCREEN_WIDTH_HEIGHT_RATIO,
        1.0f32,
        -1.0/SCREEN_WIDTH_HEIGHT_RATIO,
    );

    let mut image = RgbImage::new(SCREEN_WIDTH, SCREEN_HEIGHT);

    // let (spheres, lights) = scenes::default_scene();
    // let (spheres, lights) = scenes::default_scene_two_lights();
    // let (spheres, lights) = scenes::many_spheres_scene();
    let (spheres, lights) = scenes::many_spheres_two_lights_scene();

    let x_step_length = (screen_right-screen_left) / (SCREEN_WIDTH as f32);
    let y_step_length = (screen_top-screen_bottom) / (SCREEN_HEIGHT as f32);
    for row in 0..SCREEN_HEIGHT {
        let y_coord = screen_top - (y_step_length * (row as f32));
        for col in 0..SCREEN_WIDTH {
            let x_coord = x_step_length * (col as f32) + screen_left;

            let mut color = Vec3::new(0.0, 0.0, 0.0);


            let pixel_position = Vec3::new(x_coord, y_coord, SCREEN_DEPTH);


            for light in &lights {
                let mut reflection: f32 = 1.0;
                let mut origin_position = camera_position;
                let vector_from_origin_to_pixel = pixel_position - origin_position;
                let mut direction_unit_vector = vector_from_origin_to_pixel.normalize();
                for _ in 0..MAX_REFLECTION_DEPTH {
                    // if i == 0 {continue;}
                    // get distance to nearest object intersection point from origin to screen
                    let (nearest_sphere, distance) =
                        nearest_intersected_sphere(&spheres, &origin_position, &direction_unit_vector);

                    if nearest_sphere.is_none() {
                        break;
                    }

                    let nearest_sphere = nearest_sphere.unwrap();

                    let intersection = origin_position + distance * direction_unit_vector;

                    // to prevent detecting itself in shadows
                    let normal_to_surface = (intersection - nearest_sphere.center).normalize();
                    let shifted_point = intersection + 1e-5 * normal_to_surface;

                    // unit vector from intersection to light
                    let intersection_to_light = (light.center - shifted_point).normalize();

                    // get distance to nearest object between the shifted intersection point and the light
                    let (_, min_distance_to_sphere_between_self_and_light) = nearest_intersected_sphere(
                        &spheres, &shifted_point, &intersection_to_light);

                    // get the distance between the shifted intersection point and the light
                    let intersection_to_light_distance = (light.center - intersection).length();

                    // if intersection point closer to nearest object between it and light than to the light, then it is being shadowed
                    let is_shadowed = min_distance_to_sphere_between_self_and_light < intersection_to_light_distance;

                    if is_shadowed {
                        break;
                    }

                    let intersection_to_origin = (origin_position - intersection).normalize();

                    let mut rgb_illumination = Vec3::new(0.0, 0.0, 0.0);

                    rgb_illumination += nearest_sphere.ambient * light.ambient;

                    rgb_illumination += nearest_sphere.diffuse * light.diffuse * intersection_to_light.dot(normal_to_surface);

                    rgb_illumination +=
                        (normal_to_surface.dot((intersection_to_light + intersection_to_origin).normalize())).powf(
                            nearest_sphere.shininess / 4.0
                        ) * nearest_sphere.specular * light.specular ;
                    
                    color += rgb_illumination * reflection;
                    reflection *= nearest_sphere.reflection;
                    origin_position = shifted_point;
                    direction_unit_vector = reflected(&direction_unit_vector, &normal_to_surface);

                }


            }
            
            let rgb_color = (color * 255.0).clamp(Vec3::new(0.0, 0.0, 0.0), Vec3::new(255.0, 255.0, 255.0));

            let rgb_slice = [rgb_color.x as u8, rgb_color.y as u8, rgb_color.z as u8];

            image.put_pixel(col, row, Rgb(rgb_slice));
        }
        println!("progress: {:.2}%", ((row+1) as f32/SCREEN_HEIGHT as f32) * 100.0);
    }

    // image.save_with_format("./out/default-2lights.png", image::ImageFormat::Png).expect("save image error");
    image.save_with_format("./out/manyspheres-2lights.png", image::ImageFormat::Png).expect("save image error");


}
