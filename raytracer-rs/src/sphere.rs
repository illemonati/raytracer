use glam::Vec3;



pub struct Sphere {
    pub radius: f32,
    pub center: Vec3,
    pub ambient: Vec3,
    pub diffuse: Vec3,
    pub specular: Vec3,
    pub shininess: f32,
    pub reflection: f32,
}


impl Sphere {
    pub fn new(radius: f32, center: Vec3, ambient: Vec3, diffuse: Vec3, specular: Vec3, shininess: f32, reflection: f32) -> Sphere {
        Sphere {
            radius,
            center,
            ambient,
            diffuse,
            specular,
            shininess,
            reflection,
        }
    }

    pub fn first_intersection(&self, ray_origin: &Vec3, ray_direction: &Vec3) -> f32 {
        let a = 1.0; // ray_direction**2 is always equal to 1 in this case
        let b = 2.0 * ray_direction.dot(*ray_origin - self.center);
        let c = (*ray_origin - self.center).length().powi(2) - self.radius.powi(2);

        let discriminant = b.powi(2) - 4.0 * a * c;
        if discriminant < 0.0 {
            // negative means invalid
            return -1.0;
        }

        let t0 = (-b + discriminant.sqrt()) / (2.0 * a);
        let t1 = (-b - discriminant.sqrt()) / (2.0 * a);

        return f32::min(t0, t1);

    }
}

