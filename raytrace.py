#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

screen_width = 1800
screen_height = 1400

camera_position = np.array([0, 0, 1])

width_height_ratio = screen_width / screen_height

# 1 is no reflection
max_reflection_depth = 3


screen_left, screen_top, screen_right, screen_bottom = (
    -1,
    1/width_height_ratio,  # 1 * height / width
    1,
    -1/width_height_ratio
)

# on the third axis the screen is at position 0
screen_depth = 0

# 3 is for r g b
image = np.zeros((screen_height, screen_width, 3))


# center is center of sphere
# radius is radius of sphere
# ambient, diffuse, specular and shininess are based on the Blinn-Phong model
# spheres = [
#     {'center': np.array([-0.2, 0, -1]), 'radius': 0.7, 'ambient': np.array([0.1, 0, 0]), 'diffuse': np.array(
#         [0.7, 0, 0]), 'specular': np.array([1, 1, 1]), 'shininess': 100, 'reflection': 0.5},
#     {'center': np.array([0.1, -0.3, 0]), 'radius': 0.1, 'ambient': np.array([0.1, 0, 0.1]), 'diffuse': np.array(
#         [0.7, 0, 0.7]), 'specular': np.array([1, 1, 1]), 'shininess': 100, 'reflection': 0.5},
#     {'center': np.array([-0.3, 0, 0]), 'radius': 0.15, 'ambient': np.array([0, 0.1, 0]), 'diffuse': np.array(
#         [0, 0.6, 0]), 'specular': np.array([1, 1, 1]), 'shininess': 100, 'reflection': 0.5},
#     {'center': np.array([0, -9000, 0]), 'radius': 9000 - 0.7, 'ambient': np.array([0.1, 0.1, 0.1]),
#      'diffuse': np.array([0.6, 0.6, 0.6]), 'specular': np.array([1, 1, 1]), 'shininess': 100, 'reflection': 0.5}
# ]
spheres = [
    {'center': np.array([0, -9000, 0]), 'radius': 9000 - 0.7, 'ambient': np.array([0.1, 0.1, 0.1]),
     'diffuse': np.array([0.6, 0.6, 0.6]), 'specular': np.array([1, 1, 1]), 'shininess': 100, 'reflection': 0.5}
]

for row, x_coord in enumerate(np.linspace(-0.8, 0.8, 3)):
    for col, y_coord, in enumerate(np.linspace(-0.5, 0.5, 3)):
        for file, z_coord, in enumerate(np.linspace(0, -0.8, 2)):
            spheres.append({
                'center': np.array([x_coord, y_coord, z_coord]),
                'radius': 0.2,
                'specular': np.array([1, 1, 1]),
                'ambient': np.random.uniform(low=0, high=0.1, size=(3)),
                'diffuse': np.random.rand(3),
                'shininess': 100,
                'reflection': 0.5,
            })


light_position = np.array([5, 5, 5])
light_ambient = np.array([1, 1, 1])
light_diffuse = np.array([1, 1, 1])
light_specular = np.array([1, 1, 1])


# gets the unit vector from a vector with the same direction
# norm is length of vector
def get_direction_vector(vector):
    return vector / np.linalg.norm(vector)


# vector reflection across axis
def reflected(vector, axis):
    return vector - 2 * np.dot(vector, axis) * axis


def first_sphere_interaction(center, radius, ray_origin, ray_direction):
    a = 1  # ray_direction**2 is always equal to 1 in this case
    b = 2 * np.dot(ray_direction, (ray_origin - center))
    c = np.linalg.norm(ray_origin - center) ** 2 - radius ** 2
    discriminant = b**2 - 4 * a * c
    if discriminant < 0:
        return None
    t0 = (-b + np.sqrt(discriminant)) / (2 * a)
    t1 = (-b - np.sqrt(discriminant)) / (2 * a)
    return np.minimum(t0, t1)


def nearest_intersected_sphere(spheres, ray_origin, ray_direction):
    distances = [(sphere, first_sphere_interaction(
        sphere['center'], sphere['radius'], ray_origin, ray_direction)) for sphere in spheres]
    nearest_sphere = None
    min_distance = np.inf
    for sphere, distance in distances:
        if distance and distance >= 0 and distance < min_distance:
            nearest_sphere = sphere
            min_distance = distance

    return nearest_sphere, min_distance


# get equally spaced out values for row and column in the screen
for row, y_coord in enumerate(np.linspace(screen_top, screen_bottom, screen_height)):
    for col, x_coord, in enumerate(np.linspace(screen_left, screen_right, screen_width)):
        color = np.zeros((3))
        reflection = 1
        origin_position = camera_position

        pixel_position = np.array((x_coord, y_coord, screen_depth))

        vector_from_origin_to_pixel = pixel_position - origin_position
        direction_unit_vector = get_direction_vector(
            vector_from_origin_to_pixel
        )

        for _ in range(max_reflection_depth):
            # get distance to nearest object intersection point from origin to screen
            nearest_sphere, min_distance = nearest_intersected_sphere(
                spheres, origin_position, direction_unit_vector)

            if nearest_sphere is None:
                break

            intersection = origin_position + min_distance * direction_unit_vector

            # to prevent detecting itself in shadows
            normal_to_surface = get_direction_vector(
                intersection - nearest_sphere['center'])
            shifted_point = intersection + 1e-5 * normal_to_surface

            # unit vector from intersection to light
            intersection_to_light = get_direction_vector(
                light_position - shifted_point)

            # get distance to nearest object between the shifted intersection point and the light
            _, min_distance_to_sphere_between_self_and_light = nearest_intersected_sphere(
                spheres, shifted_point, intersection_to_light)

            # get the distance between the shifted intersection point and the light
            intersection_to_light_distance = np.linalg.norm(
                light_position - intersection)

            # if intersection point closer to nearest object between it and light than to the light, then it is being shadowed
            is_shadowed = min_distance_to_sphere_between_self_and_light < intersection_to_light_distance

            if is_shadowed:
                break

            intersection_to_origin = get_direction_vector(
                origin_position - intersection)

            # add colors based on blinn-phong
            rgb_illumination = np.zeros((3))

            # ambient is color in absence of light
            rgb_illumination += nearest_sphere['ambient'] * light_ambient

            # diffuse is normal color
            rgb_illumination += nearest_sphere['diffuse'] * light_diffuse * np.dot(
                intersection_to_light, normal_to_surface)

            # # specular is color of shiny parts
            rgb_illumination += nearest_sphere['specular'] * \
                light_specular * np.dot(normal_to_surface, get_direction_vector(
                    intersection_to_origin+intersection_to_light)) ** (nearest_sphere['shininess'] / 4)

            color += reflection * rgb_illumination
            reflection *= nearest_sphere['reflection']

            origin_position = shifted_point
            direction_unit_vector = reflected(
                direction_unit_vector, normal_to_surface)

        # clip to make sure in range
        image[row, col] = np.clip(color, 0, 1)

    print(f"progress: {((row+1)/screen_height*100):.2f}%")


plt.imsave('out/manyspheres.png', image)
